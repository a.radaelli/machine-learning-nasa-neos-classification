# Machine Learning - NASA NEOs classification

## First things first
This project is the result of a Machine Learning course I took during my associate's degree.
Thanks to this experience I learnt:
- strong foundation in coding, math, ML theory, and how to build my own ML project from start to finish.
- Different methods and models of Supervised and Unsupervised learning (KNN, SVM, Tree, RF, NN etc...)

## Description
This project in particular, tries to classify Near the Earth orbiting Object in hazardous / non-hazardous in case of a collision with the our planet.

## K-Fold
In addition to the code itself, I added a K-fold algorithm that determines the best parameters for a RF classifier.
