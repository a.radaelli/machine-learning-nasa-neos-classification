import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import sklearn.metrics as metrics

from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import plot_tree
from sklearn.ensemble import RandomForestClassifier
from keras.models import Sequential
from keras.layers import Dense
from sklearn.metrics import accuracy_score

# importing data from repository
df = pd.read_csv('./data/nasa.csv', header=0)

# checking if dataset has labels, shape, column type, data summary, unique, duplicates and nan
print(f'\n------------> DATASET HEAD <------------\n{df.head()}')
print(f'\n------------> DATASET INFO <------------')
print(df.info())
print(f'\n------------> DATASET DESCRIPTION <------------\n{df.describe().round(2)}')
print(f'\n------------> UNIQUE VALUES <------------\n{df.nunique().sort_values()}')
print(
    f'\n------------> DUPLICATED ROWS <------------\n{df.duplicated().sum()}, ({np.round(100 * df.duplicated().sum() / len(df), 2)}%)')
print(f'\n------------> NAN VALUES <------------\n{df.isnull().sum()}')

# dataset visualizations
colors = sns.dark_palette("#69d", reverse=True)

# correlation matrix
df_corr = df.drop(labels=['id', 'name', 'orbiting_body', 'sentry_object'], axis=1)
corrMatrix = df_corr.corr()
fig = plt.figure()
fig.patch.set_facecolor('#2B2B2B')
ax = plt.axes(facecolor="#2B2B2B")
ax.xaxis.label.set_color('white')
ax.yaxis.label.set_color('white')
ax.tick_params(colors='white', which='both')
title_obj = plt.title('Correlation Matrix')
plt.setp(title_obj, color='white')
c = sns.diverging_palette(h_neg=275, h_pos=249.3, s=66.6, l=62.4, as_cmap=True, center='dark')
sns.heatmap(corrMatrix, annot=True, cmap=c, vmin=-1, square=True)
plt.show()

# how many hazardous asteroids?
plt.rcParams['text.color'] = 'white'
plt.figure(facecolor='#2B2B2B')
haz = df.hazardous.sum()
nonhaz = df.shape[0] - haz
plt.pie([haz, nonhaz], labels=['Hazardous', 'Non-Hazardous'], autopct='%1.0f%%', colors=['#861D8F', '#69d'])
plt.show()

# scatter magnitude / diameter
plt.figure(facecolor='#2B2B2B')
ax = plt.axes(facecolor="#2B2B2B")
ax.spines['bottom'].set_color('white')
ax.spines['top'].set_color('white')
ax.spines['right'].set_color('white')
ax.spines['left'].set_color('white')
ax.xaxis.label.set_color('white')
ax.yaxis.label.set_color('white')
ax.tick_params(colors='white', which='both')
plt.scatter(df["absolute_magnitude"], df["est_diameter_min"], color=colors[0])
plt.xlabel("absolute_magnitude")
plt.ylabel("est_diameter_min")
m, b = np.polyfit(df["absolute_magnitude"], df["est_diameter_min"], 1)
plt.plot(df["absolute_magnitude"], m * df["absolute_magnitude"] + b, c='#861D8F', linewidth=0.7)
plt.show()

# scatter magnitude / velocity
plt.figure(facecolor='#2B2B2B')
ax = plt.axes(facecolor="#2B2B2B")
ax.spines['bottom'].set_color('white')
ax.spines['top'].set_color('white')
ax.spines['right'].set_color('white')
ax.spines['left'].set_color('white')
ax.xaxis.label.set_color('white')
ax.yaxis.label.set_color('white')
ax.tick_params(colors='white', which='both')
plt.scatter(df["absolute_magnitude"], df["relative_velocity"], s=1, color=colors[0])
plt.xlabel("absolute_magnitude")
plt.ylabel("relative_velocity")
m, b = np.polyfit(df["absolute_magnitude"], df["relative_velocity"], 1)
plt.plot(df["absolute_magnitude"], m * df["absolute_magnitude"] + b, c='#861D8F', linewidth=0.7)
plt.show()


# pairplot
sns.pairplot(df, hue="hazardous")
plt.show()

# magnitude distribution
haz = df.loc[df['hazardous'] == True]
non_haz = df.loc[df['hazardous'] == False]
haz = haz['absolute_magnitude'].to_numpy()
non_haz = non_haz['absolute_magnitude'].to_numpy()
plt.hist(non_haz, bins=120, label='not hazardous', color=colors[0])
plt.hist(haz, bins=70, label='hazardous', color='#861D8F')
plt.legend(loc='upper left')
plt.xlabel("absolute_magnitude")
plt.show()

# diameter min distribution
plt.hist(df["est_diameter_min"], bins=50, color = colors[0], range=(0,1))
plt.xlabel("est_diameter_min")
plt.show()

# max dimension distribution
plt.hist(df["est_diameter_max"], bins=50, color = colors[0], range=(0,1))
plt.xlabel("est_diameter_max")
plt.show()

# velocity distribution
plt.hist(df["relative_velocity"], bins=50, color = colors[0])
plt.xlabel("relative_velocity")
plt.show()

# scatter diameter min/velocity
plt.scatter(df["est_diameter_min"], df["relative_velocity"], s=1, color = colors[0])
plt.xlabel("est_diameter_min")
plt.ylabel("relative_velocity")
plt.show()

# scatter dim max/velocity
plt.scatter(df["est_diameter_max"], df["relative_velocity"], s=1, color = colors[0])
#plt.xlim(0, 10)
plt.xlabel("est_diameter_max")
plt.ylabel("relative_velocity")
plt.show()

# dropping rumors columns
print(df.columns)
df = df.drop(labels=['id', 'name', 'orbiting_body', 'sentry_object', 'est_diameter_max'], axis=1)

# for loop with x=magnitude, hue=hazardous
for i in df.columns[0:-2]:
    ax = plt.subplots()
    plt.figure(facecolor='#2B2B2B')
    ax = plt.axes(facecolor="#2B2B2B")
    ax.spines['bottom'].set_color('white')
    ax.spines['top'].set_color('white')
    ax.spines['right'].set_color('white')
    ax.spines['left'].set_color('white')
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.tick_params(colors='white', which='both')
    colors_h = {0: '#69d', 1: '#861D8F'}
    grouped = df.groupby('hazardous')
    for key, group in grouped:
        group.plot(ax=ax, kind='scatter', x='absolute_magnitude', y=i, label=key, color=colors_h[key], legend=False)
    plt.show()


# dropping rumors columns
print(df.columns)
df = df.drop(labels=['id', 'name', 'orbiting_body', 'sentry_object', 'est_diameter_max'], axis=1)

# encoding column hazardous
df['hazardous'] = df['hazardous'].replace('True', 1)
df['hazardous'] = df['hazardous'].replace('False', 0)

# slicing dataset and transformation from pandas df to numpy array
x = df.loc[:, df.columns != 'hazardous'].to_numpy(dtype='float64')
y = df['hazardous'].to_numpy(dtype='float64')

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=1997)

# min-max scaling columns
scaler = MinMaxScaler()
x_train = scaler.fit_transform(x_train)
x_test = scaler.transform(x_test)

# KNN 5
knn = KNN()
knn.fit(x_train, y_train)
knn_pred = knn.predict(x_test)
knn_accuracy = accuracy_score(y_test, knn_pred)
print(f'KNN Accuracy: {100 * knn_accuracy: .2f}%')

# SVM LINEAR
SVC_lin = SVC(kernel='linear')
SVC_lin.fit(x_train, y_train)
svm_pred = SVC_lin.predict(x_test)
accuracy = accuracy_score(y_test, svm_pred)
print(f'Linear SVM Accuracy: {100 * accuracy: .2f}%')

# SVM RBF
SVC_rbf = SVC(kernel='rbf')
SVC_rbf.fit(x_train, y_train)
svc_pred = SVC_rbf.predict(x_test)
svc_accuracy = accuracy_score(y_test, svc_pred)
print(f'RBF SVC Accuracy DEFAULT: {100 * svc_accuracy: .2f}%')

# ID3 Gini
gini_model = DecisionTreeClassifier()
gini_model.fit(x_train, y_train)
y_gini_model = gini_model.predict(x_test)
gini_acc = accuracy_score(y_test, y_gini_model)
print(f'GINI accuracy ID3: {gini_acc * 100:.2f}%')
# plt.figure(figsize=(12, 12))
# plot_tree(gini_model, filled=True, fontsize=6, feature_names=df.columns)
# plt.show()

# ID3 Entropy
ent_model = DecisionTreeClassifier(criterion='entropy')
ent_model.fit(x_train, y_train)
y_ent_model = ent_model.predict(x_test)
ent_acc = accuracy_score(y_test, y_ent_model)
print(f'ENTROPY accuracy ID3: {ent_acc * 100:.2f}%')
# plt.figure(figsize=(12, 12)) # width, height
# plot_tree(ent_model, filled=True, fontsize=6, feature_names=df.columns)
# plt.show()

# Random Forest classifier (10 Trees)
rf_model = RandomForestClassifier(n_estimators=10)
rf_model.fit(x_train, y_train)
y_rf_model = rf_model.predict(x_test)
rf_acc = accuracy_score(y_test, y_rf_model)
print(f'RANDOM FOREST accuracy: {rf_acc * 100:.2f}%')

# Neural Network 1
classifier = Sequential()
classifier.add(Dense(20, input_dim=4, activation='relu'))
classifier.add(Dense(30, activation='relu'))
classifier.add(Dense(1, activation='sigmoid'))
classifier.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
classifier.summary()

classifier.fit(x_train, y_train, batch_size=100, epochs=30)
_, accuracy = classifier.evaluate(x_test, y_test)
print(f'NN 1 Accuracy: {accuracy * 100: %.2f}%')

# Neural Network 2
c = 150
classifier = Sequential()
classifier.add(Dense(c, input_dim=4, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(c, activation='relu'))
classifier.add(Dense(1, activation='sigmoid'))
classifier.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
classifier.summary()

classifier.fit(x_train, y_train, batch_size=1000, epochs=20)
_, accuracy = classifier.evaluate(x_test, y_test)
print(f'NN 2 Accuracy: {accuracy * 100 :.2f}%')

# confusion matrix on RF
ConfMat = confusion_matrix(y_test, y_rf_model)
fig = plt.figure()
fig.patch.set_facecolor('#2B2B2B')
ax = plt.axes(facecolor="#2B2B2B")
ax.spines['bottom'].set_color('white')
ax.spines['top'].set_color('white')
ax.spines['right'].set_color('white')
ax.spines['left'].set_color('white')
ax.xaxis.label.set_color('white')
ax.yaxis.label.set_color('white')
ax.tick_params(colors='white', which='both')
c = sns.dark_palette("#69d")
title_obj = plt.title('Confusion Matrix')
plt.setp(title_obj, color='white')
sns.heatmap(ConfMat, annot=True, cmap=c, square=True)
ax.set_xlabel('Predicted Values')
ax.set_ylabel('True Values')
plt.show()

# ROC Curve
probs = rf_model.predict_proba(x_test)
preds = probs[:, 1]
fpr, tpr, threshold = metrics.roc_curve(y_test, preds)
roc_auc = metrics.auc(fpr, tpr)
fig = plt.figure()
fig.patch.set_facecolor('#2B2B2B')
ax = plt.axes(facecolor="#2B2B2B")
ax.spines['bottom'].set_color('white')
ax.spines['top'].set_color('white')
ax.spines['right'].set_color('white')
ax.spines['left'].set_color('white')
ax.xaxis.label.set_color('white')
ax.yaxis.label.set_color('white')
ax.tick_params(colors='white', which='both')
title_obj = plt.title('ROC Curve')
plt.setp(title_obj, color='white')
plt.plot(fpr, tpr, "#69d")
plt.plot([0, 1], [0, 1], 'w', linewidth='0.5')
plt.xlim([0, 1])
plt.ylim([0, 1])
ax.set_xlabel('Predicted Values')
ax.set_ylabel('True Values')
plt.show()
