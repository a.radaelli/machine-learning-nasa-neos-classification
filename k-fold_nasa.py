import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
from datetime import timedelta
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

# importing data from repository
df = pd.read_csv('./data/nasa.csv', header=0)

# dropping rumors columns
df = df.drop(labels=['id', 'name', 'orbiting_body', 'sentry_object', 'est_diameter_max'], axis=1)

# encoding column hazardous
df['hazardous'] = df['hazardous'].replace('True', 1)
df['hazardous'] = df['hazardous'].replace('False', 0)

# slicing dataset and transformation from pandas df to numpy array
x = df.loc[:, df.columns != 'hazardous'].to_numpy(dtype='float64')
y = df['hazardous'].to_numpy(dtype='float64')

# K fold
splits = 5
kf = KFold(n_splits=splits, shuffle=False)

'''
loop= the same x_train goes through all the different RF configuration
spin= a single model with a specific configuration

for each loop a list of accuracies is saved in 'all_accuracies'
in 'max_accuracies' the best accuracy for each loop is saved
in 'max_model' the best model for each loop is saved
'cursor' allow to access the correct list in 'all_accuracies'
'''
loop = 0  # 1 loop: the same x_train pass through all different RF configurations
max_accuracies = []  # max accuracy for one loop
max_model = []  # best model for one loop
all_accuracies = []  # list of list
cursor = 0  # loop cursor
for train_index, test_index in kf.split(x):
    loop_time = time.time()
    x_train, x_test = x[train_index], x[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # min-max scaling columns
    scaler = MinMaxScaler()
    x_train = scaler.fit_transform(x_train)
    x_test = scaler.transform(x_test)

    # Random Forest classifier
    loop = loop + 1
    trees = [10, 100, 150, 250]
    crits = ['gini', 'entropy']
    dept = [1, 10, None]
    leaves = [10, 20, 50, None]
    models = [RandomForestClassifier(n_estimators=tree, criterion=cri, max_depth=m, max_leaf_nodes=leaf)
              for tree in trees
              for cri in crits
              for m in dept
              for leaf in leaves]

    spin = 0  # 1 spin: 1 model
    loop_accuracies = []  # accuracy list for one loop (references model)
    lspin_time=[]
    for model in models:
        spin_time = time.time()
        model.fit(x_train, y_train)
        y_pred = model.predict(x_test)
        rf_acc = accuracy_score(y_test, y_pred)
        loop_accuracies.append(rf_acc)
        spin = spin + 1
        spin_end = time.time()
        lspin_time.append(spin_end - spin_time)
        if spin <= 9:
            print(
                f'Loop: {loop}/{splits}  Spin: {spin}/{len(trees) * len(crits) * len(dept) * len(leaves)}   Accuracy:{rf_acc * 100 : .2f}%  Time: {timedelta(seconds=spin_end - spin_time)}')
        else:
            print(
                f'Loop: {loop}/{splits}  Spin: {spin}/{len(trees) * len(crits) * len(dept) * len(leaves)}  Accuracy:{rf_acc * 100 : .2f}%  Time: {timedelta(seconds=spin_end - spin_time)}')
    all_accuracies.append(loop_accuracies)
    loop_end = time.time()

    # find best model in loop
    max_index = 0
    my_max = 0
    for i, num in enumerate(all_accuracies[cursor]):
        if num > my_max:
            my_max = num
            max_index = i
    cursor += 1
    max_accuracies.append(my_max)
    max_model.append(models[max_index])
    print(
        f'Loop {loop} best model: {models[max_index]} with {my_max * 100 :.2f}% accuracy and time: {timedelta(seconds=loop_end - loop_time)}\n')
    plt.plot(lspin_time)
    plt.title(f'Time Loop {loop}')
    plt.savefig(f'Time Loop {loop}.png')
    plt.close()

# giving mean and std of accuracies
array_accuracies = np.array(all_accuracies)
print(f'\nAccuracies mean: {100 * np.mean(array_accuracies):.2f}%')
print(f'Accuracies standard deviation: {100 * np.std(array_accuracies):.2f}%')

# find best model above all
max_index = 0
my_max = 0
for i, num in enumerate(max_accuracies):
    if num > my_max:
        my_max = num
        max_index = i
print(f'\nBest model: {max_model[max_index]} with {my_max * 100 :.2f}% accuracy')

# plotting accuracies
for lista in all_accuracies:
    plt.plot(lista)
plt.savefig('Accuracies plot.png')
